package net.thumbtack.async.server.dto;

public class AdditionDtoResponse {
    private int sum;

    public AdditionDtoResponse(int sum) {
        this.sum = sum;
    }

    public int getSum() {
        return sum;
    }
}
