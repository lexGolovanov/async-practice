package net.thumbtack.async.server.dto;

import java.util.Objects;

public class AdditionDtoRequest {
    private int x;
    private int y;

    public AdditionDtoRequest(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdditionDtoRequest)) return false;
        AdditionDtoRequest that = (AdditionDtoRequest) o;
        return getX() == that.getX() &&
                getY() == that.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }
}
