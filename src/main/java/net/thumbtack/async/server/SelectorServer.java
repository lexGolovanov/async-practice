package net.thumbtack.async.server;

import com.google.gson.Gson;
import net.thumbtack.async.server.dto.AdditionDtoRequest;
import net.thumbtack.async.server.dto.AdditionDtoResponse;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class SelectorServer {
    private static Gson gson = new Gson();
    private static final String HOST = "localhost";
    private static final int PORT = 5000;
    private static final int BASE_RESPONSE_LENGTH = 35;
    private static final String HTTP_RESPONSE = "HTTP/1.1 200 OK\n" +
                    "Content-Length: %s\n" +
                    "Content-Type: text/html\n" +
                    "Connection: Closed\n\n" +
                    "<html>\n" +
                    "<body>\n" +
                    "<h1>%s</h1>\n" +
                    "</body>\n" +
                    "</html>";

    private static AdditionDtoRequest handleTwoNumbers(final SocketChannel client) throws IOException {
        final ByteBuffer buffer = ByteBuffer.allocate(1024);
        int n = client.read(buffer);
        if (n == -1) {
            return null;
        }

        String requestString = new String(buffer.array()).trim();
        if (!requestString.contains("POST")) {
            return null;
        }

        requestString = requestString.substring(requestString.lastIndexOf("{") - 1);
        log("Message received: " + requestString);
        return gson.fromJson(requestString, AdditionDtoRequest.class);
    }

    private static ByteBuffer createSumResponse(final SocketChannel client, AdditionDtoRequest request) {
        int sum = request.getX() + request.getY();
        log("Sum is: " + sum);
        final AdditionDtoResponse response = new AdditionDtoResponse(sum);

        final String sumJson = gson.toJson(response);
        final String responseString = String.format(
                HTTP_RESPONSE,
                BASE_RESPONSE_LENGTH + sumJson.length(), sumJson
        );
        return StandardCharsets.UTF_8.encode(responseString);
    }

    public static void main(String[] args) throws IOException {
        Selector selector = Selector.open();
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.bind(new InetSocketAddress(HOST, PORT));
        serverChannel.configureBlocking(false);
        serverChannel.register(selector, SelectionKey.OP_ACCEPT, null);

        final Map<String, AdditionDtoRequest> requestMap = new HashMap<>();

        while (true) {
            selector.select();
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = keys.iterator();

            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                if (key.isAcceptable()) {
                    final SocketChannel clientChannel = serverChannel.accept();
                    clientChannel.configureBlocking(false);
                    clientChannel.register(selector, SelectionKey.OP_READ);
                    log("Connection Accepted: " + clientChannel.getLocalAddress() + "\n");
                } else if (key.isReadable()) {
                    final SocketChannel client = (SocketChannel) key.channel();

                    AdditionDtoRequest request = handleTwoNumbers(client);
                    if (request == null) {
                        client.close();
                    } else {
                        final String remoteAddress = client.getRemoteAddress().toString();
                        requestMap.putIfAbsent(remoteAddress, request);
                        key.interestOps(SelectionKey.OP_WRITE);
                    }
                } else if (key.isWritable()) {
                    final SocketChannel client = (SocketChannel) key.channel();

                    final String requestMapKey = client.getRemoteAddress().toString();
                    final AdditionDtoRequest request = requestMap.get(requestMapKey);

                    ByteBuffer writeBuffer = createSumResponse(client, request);
                    client.write(writeBuffer);
                    if (!writeBuffer.hasRemaining()) {
                        writeBuffer.compact();
                        key.interestOps(SelectionKey.OP_READ);
                    }

                    client.close();
                    requestMap.remove(requestMapKey);
                }
                iterator.remove();
            }
        }
    }

    private static void log(String str) {
        System.out.println(str);
    }
}
