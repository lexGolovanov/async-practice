package net.thumbtack.async.correlation;

public class MeanPair<V> {
    private final V xMean;
    private final V yMean;

    public MeanPair(final V xMean, final V yMean) {
        this.xMean = xMean;
        this.yMean = yMean;
    }

    public V getMeanX() {
        return xMean;
    }

    public V getMeanY() {
        return yMean;
    }
}
