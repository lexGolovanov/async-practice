package net.thumbtack.async.correlation;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class AsyncCorrelation {
    public static void main(String[] args) {
        final List<Double> xSample = Arrays.asList(1.0, 3.0, 11.0);
        final List<Double> ySample = Arrays.asList(1.0, 4.0, 12.0);

        final CompletableFuture<Double> meanOfX = CompletableFuture.supplyAsync(
                        () -> {
                            double sum = 0;
                            for (double elem : xSample) {
                                sum += elem;
                            }
                            return sum / xSample.size();
                        }
                );
        final CompletableFuture<Double> meanOfY = CompletableFuture.supplyAsync(
                        () -> {
                            double sum = 0;
                            for (double elem : ySample) {
                                sum += elem;
                            }
                            return sum / ySample.size();
                        }
                );

        final CompletableFuture<MeanPair<Double>> createMeanPair = meanOfX.thenCombine(
                meanOfY, (xMean, yMean) -> new MeanPair<>(xMean, yMean)
        );

        final CompletableFuture<Double> covariation = createMeanPair.thenApplyAsync(
                (pair) -> {
                    double cov = 0;
                    for (int i = 0; i < xSample.size(); i++) {
                        cov += (xSample.get(i) - pair.getMeanX()) * (ySample.get(i) - pair.getMeanY());
                    }
                    return cov;
                }
        );

        final CompletableFuture<Double> xDevi = createMeanPair.thenApplyAsync(
                (pair) -> {
                    double sum = 0;
                    for (double xValue : xSample) {
                        sum += (xValue - pair.getMeanX()) * (xValue - pair.getMeanX());
                    }
                    return sum;
                }
        );
        final CompletableFuture<Double> yDevi = createMeanPair.thenApplyAsync(
                (pair) -> {
                    double sum = 0;
                    for (double yValue : ySample) {
                        sum += (yValue - pair.getMeanY()) * (yValue - pair.getMeanY());
                    }
                    return sum;
                }
        );
        final CompletableFuture<Double> deviation = xDevi
                .thenCombine(yDevi, (xValue, yValue) -> xValue * yValue)
                .thenApply(mul -> Math.sqrt(mul));

        final CompletableFuture<Double> correlationCoefficient = covariation.thenCombine(
                deviation,
                (cov, devi) -> cov / devi
        );
        try {
            System.out.println(correlationCoefficient.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
