package net.thumbtack.async.files;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class FileTask {
    private static List<Integer> readNumbersFromFile(final String filename) {
        System.out.println("Reading from " + filename + " thread: " + Thread.currentThread().getId());
        final List<Integer> result = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(filename), StandardCharsets.UTF_8)
        )) {
            String line;
            while ((line = reader.readLine()) != null) {
                result.add(Integer.parseInt(line));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private static void writeNumbersToFile(final String filename, final List<Integer> numbers) {
        System.out.println("Writing to " + filename + " thread: " + Thread.currentThread().getId());
        try (BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(filename), StandardCharsets.UTF_8)
        )) {
            for (int number : numbers) {
                writer.write(number + "\n");
            }
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        final CompletableFuture<List<Integer>> firstFileReader = CompletableFuture
                .supplyAsync(() -> readNumbersFromFile("file1.txt"));
        final CompletableFuture<List<Integer>> secondFileReader = CompletableFuture
                .supplyAsync(() -> readNumbersFromFile("file2.txt"));

        final CompletableFuture<Void> resultFuture1 = firstFileReader.thenCombineAsync(
                secondFileReader,
                (list1, list2) -> {
                    final List<Integer> summed = new ArrayList<>();
                    for (int i = 0; i < list1.size(); i++) {
                        summed.add(list1.get(i) + list2.get(i));
                    }
                    return summed;
                }
        )
                .thenAccept((resultList) -> writeNumbersToFile("file_sum.txt", resultList));
        final CompletableFuture<Void> resultFuture2 = firstFileReader.thenCombineAsync(
                secondFileReader,
                (list1, list2) -> {
                    final List<Integer> multiplied = new ArrayList<>();
                    for (int i = 0; i < list1.size(); i++) {
                        multiplied.add(list1.get(i) * list2.get(i));
                    }
                    return multiplied;
                }
        )
                .thenAccept((resultList) -> writeNumbersToFile("file_mul.txt", resultList));

        try {
            resultFuture1.get();
            resultFuture2.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
